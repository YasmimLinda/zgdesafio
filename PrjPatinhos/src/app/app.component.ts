import { Component } from '@angular/core';
import { Patinho } from './models/patinho.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  patinho : Patinho = new Patinho();
  numeroPatinhos: number = 0;
  mensagemInvalido: string = '';
  letraMusica: string = '';

  // Função chamada ao clicar no botão "Cantar"
  cantar() {
    if (!this.validarNumeroPatinhos(this.patinho.numeroPatinhos)) {
      this.mensagemInvalido = "Número inválido, digite novamente.";
      this.letraMusica = '';
    } else {
      this.mensagemInvalido = '';
      this.exibirLetraMusica(this.patinho.numeroPatinhos);
    }
  }

  // Função para validar o número de patinhos
  validarNumeroPatinhos(numero: number): boolean {
    return Number.isInteger(numero) && numero > 0;
  }

  // Função para exibir a letra da música
  exibirLetraMusica(numPatinhos: number) {
    this.letraMusica = ''; // Limpar a letra inicialmente

    let n = numPatinhos;

    // Função recursiva para exibir as estrofes da música
    const exibirProximaEstrofe = () => {
      if (n > 0) {
        this.letraMusica += `${n} patinho${n > 1 ? 's' : ''} foram passear\n`; // Adicionar estrofe à letra
        this.letraMusica += `Além das montanhas para brincar\n`; // Adicionar estrofe à letra
        this.letraMusica += `A mamãe gritou: "Quá, quá, quá, quá"\n`; // Adicionar estrofe à letra
        if (n > 1) {
          this.letraMusica += `Mas só ${n - 1} patinho${n > 2 ? 's' : ''} voltaram de lá\n\n`; // Adicionar estrofe à letra
        } else {
          this.letraMusica += `Mas nenhum patinho voltou de lá\n\n`; // Adicionar estrofe à letra
        }

        n--;

        setTimeout(exibirProximaEstrofe, 1000); // Aguardar 1 segundo e exibir próxima estrofe
      } else {
        this.letraMusica += `Poxa, a mamãe patinha ficou tão triste naquele dia\n`; // Adicionar estrofe à letra
        this.letraMusica += `Aonde será que estavam os seus filhotinhos?\n`; 
        this.letraMusica += `Mas essa história vai ter um final feliz\n`; 
        this.letraMusica += `Sabe por quê?\n`; 
        this.letraMusica += `A mamãe patinha foi procurar\n`;  
        this.letraMusica += `Além das montanhas, na beira do mar\n`; 
        this.letraMusica += `E os ${numPatinhos} patinho${numPatinhos > 1 ? 's' : ''} voltaram de lá`; 
      }
    };

    exibirProximaEstrofe();
  }
}
